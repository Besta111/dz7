package service;

import User.User;

import java.io.IOException;

public interface CrudUser  {
   void create(String name,
             String surname,
             int age) ;

   void update (User user) throws IOException;

//    User getById(int id);
//
//    void delete (int id);
}
