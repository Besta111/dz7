package service;

import User.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UsersRepositoryFileImpl implements CrudUser {
    @Override
    public void create(String name, String surname, int age) {
        User user = new User(name, surname, age);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("User.txt", true))) {
            writer.write(forAdd(user));
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public void update(User user) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("User.txt"));
        List<User> users = new ArrayList<>();
        User oldUser = null;

        String line = reader.readLine();
        String[] info;
        User us;

        String name;
        String surname;
        int age;
        int id;
        while (line != null) {
            info = line.split("\\|");
            id = Integer.parseInt(info[0]);
            if (id == user.getId()) {
                users.add(user);
                line = reader.readLine();
                continue;
            }
            name = info[1];
            surname = info[2];
            age = Integer.parseInt(info[3]);
            us = new User(id, name, surname, age);
            users.add(us);

            line = reader.readLine();
        }
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == user.getId()) {
                users.set(i, user);
            }
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter("User.txt"));
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == user.getId()) {
                users.set(i, user);
            }
            writer.write(forAdd(users.get(i)));
        }
        writer.flush();
        reader.close();
        writer.close();

    }

    private String forAdd(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(user.getId());
        stringBuilder.append("|");
        stringBuilder.append(user.getName());
        stringBuilder.append("|");
        stringBuilder.append(user.getSurname());
        stringBuilder.append("|");
        stringBuilder.append(user.getAge());
        stringBuilder.append("\n");

        return stringBuilder.toString();
    }
}

